﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SocialGraph.Models;

namespace SocialGraph.Util
{
    public class VkAPIWraper
    {
        private const string _clientId = "4658475";
        private const string _clientSecret = "u2IMgiGQbMmlh7sej0Ei";

        public Token APIOAuth(string code, string postBackUrl)
        {
            var url =
                String.Format(
                    "https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&code={2}&redirect_uri={3}",
                    _clientId,
                    _clientSecret,
                    code,
                    postBackUrl);
            var response = LowLevelRequest.GetMethod(url);

            var token = new Token();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                string responseToString = streamReader.ReadToEnd();

                token = JsonConvert.DeserializeObject<Models.Token>(responseToString);
            }

            return token;
        }

        public User APIUsersGet(int uid, Token token, string queryParams)
        {
            var url = String.Format(
                "https://api.vk.com/method/users.get?user_id={0}&v=5.27&access_token={1}&fields={2}",
                uid,
                token.token,
                queryParams
                );
            var response = LowLevelRequest.GetMethod(url);
            var user = new User();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                string responseToString = streamReader.ReadToEnd();
                JObject answer = JObject.Parse(responseToString);
                user = JsonConvert.DeserializeObject<User>(answer["response"].ToArray()[0].ToString());
            }
            user.id = token.id;
            user.token = token;

            return user;
        }

        public List<LiteUser> APIFriendsGet(User user)
        {
            var url = String.Format(
                "https://api.vk.com/method/friends.get?user_id={0}&order={1}&access_token={2}&fields={3}&v=5.27",
                user.id,
                "name",
                user.token.token,
                "sex"
                );
            var list = new List<LiteUser>();
            var response = LowLevelRequest.GetMethod(url);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                var responseToString = streamReader.ReadToEnd();
                var answer = JObject.Parse(responseToString);
                user.friendConter = Convert.ToInt32(answer["response"]["count"]);

                list.AddRange(answer["response"]["items"].Select(person => JsonConvert.DeserializeObject<LiteUser>(person.ToString())).Select(tmp => new LiteUser {id = tmp.id, sex = tmp.sex}));
            }

            return list;
        }

        public List<LiteUser> APIFriendsGet(string id, string token)
        {
            var url = String.Format(
                "https://api.vk.com/method/friends.get?user_id={0}&order={1}&access_token={2}&fields={3}&v=5.27",
                id,
                "name",
                token,
                "sex"                
                );
            var list = new List<LiteUser>();
            var response = LowLevelRequest.GetMethod(url);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                var responseToString = streamReader.ReadToEnd();
                var answer = JObject.Parse(responseToString);
                list.AddRange(answer["response"]["items"].Select(person => JsonConvert.DeserializeObject<LiteUser>(person.ToString())).Select(tmp => new LiteUser { id = tmp.id, sex = tmp.sex }));
            }

            return list;
        }

        public List<List<string>> APIExecuteGetFriendPack(string targets, string token)
        {
            var url = String.Format(
                    "https://api.vk.com/method/execute.getFriendPack?targets={0}&access_token={1}",
                    targets,
                    token
                    );
            var friendPack = new List<List<string>>();
            var response = LowLevelRequest.GetMethod(url);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                string responseToString = streamReader.ReadToEnd();
                var answer = JObject.Parse(responseToString);
                friendPack.AddRange(answer["response"].Select(idOfFriend => idOfFriend.Select(item => item.Value<string>()).ToList()));
            }

            return friendPack;
        }

        public JObject FriendPackToJSON(List<List<string>> friendPack,string[] friends,string user,int current)
        {
            var removedId = new string[friendPack.Count];
            var myFriendsIds = friends.ToList();
            var jObject = new JObject();

            for (int index = 0; index < friendPack.Count; index++)
            {
                friendPack[index].Remove(user);

                removedId[index] = friends[index];
                foreach (var id in removedId)
                {
                    friendPack[index].Remove(id);
                }
                var tmp = friendPack[index].Where(myFriendsIds.Contains).ToList();
                friendPack[index] = tmp;

                jObject.Add(myFriendsIds[current + index], new JArray(friendPack[index].ToArray()));
            }
            return jObject;
        }
    }
}