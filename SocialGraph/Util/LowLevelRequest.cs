﻿using System.Net;
using System.Text;

namespace SocialGraph.Util
{
    public static class LowLevelRequest
    {
        public static HttpWebResponse PostMethod(string postedDate, string postedUrl)
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(postedUrl);
            request.Method = "POST";
            request.Credentials = CredentialCache.DefaultCredentials;
            
            UTF8Encoding encoding = new UTF8Encoding();
            var bytes = encoding.GetBytes(postedDate);

            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bytes.Length;

            using (var newStream = request.GetRequestStream())
            {
                newStream.Write(bytes,0,bytes.Length);
                newStream.Close();
            }
            return (HttpWebResponse) request.GetResponse();
        }

        public static HttpWebResponse GetMethod(string getUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(getUrl);
            request.Method = "GET";
            request.Credentials = CredentialCache.DefaultCredentials;

            UTF8Encoding encoding = new UTF8Encoding();

            request.ContentType = "application/x-www-form-urlencoded";

            return (HttpWebResponse)request.GetResponse();
        }
    }
}