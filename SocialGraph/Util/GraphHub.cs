﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SocialGraph.Util
{
    public class GraphHub:Hub
    {
        public void Send(string jsonData)
        {
            Clients.Caller.updateGraph(jsonData);
        }
    }
}