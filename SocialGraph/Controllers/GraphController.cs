﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SocialGraph.Models;
using SocialGraph.Util;

namespace SocialGraph.Controllers
{
    public class GraphController : Controller
    {
        private VkAPIWraper _vkApiWraper = new VkAPIWraper();


        // GET: Graph
        public ActionResult Index()
        {
            Token token;
            token = !(User.Identity.IsAuthenticated) ? Auth() : JsonConvert.DeserializeObject<Token>(((FormsIdentity)HttpContext.User.Identity).Ticket.UserData);

            if (token == null)
            {
                return RedirectToAction("Index", "Home");
            }

            User user = _vkApiWraper.APIUsersGet(Int32.Parse(token.id),
                token, 
                "photo_50,photo_200,sex" );
            
            user.friends = _vkApiWraper.APIFriendsGet(user);

            return View(user);
        }

        public Token Auth()
        {
            if (Request.Params["code"] != null)
            {
                //Код получен -> получаем токен
                var token = _vkApiWraper.APIOAuth(Request.Params["code"],
                    Url.Action("Index", null, null, Request.Url.Scheme));

                Response.Cookies.Add(new HttpCookie(
                    "Social-Graph", SetAuthCookieString(token)));

                return token;
            }
            return null;
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            if (Request.Cookies["Social-Graph"] != null)
            {
                var c = new HttpCookie("Social-Graph") {Expires = DateTime.Now.AddDays(-1)};
                Response.Cookies.Add(c);
            }
            return RedirectToAction("Index", "Home");
        }

        private static string SetAuthCookieString(Token token)
        {
            FormsAuthentication.SetAuthCookie(token.token, false);

            string userDataString = JsonConvert.SerializeObject(token);

            var ticket = new FormsAuthenticationTicket(
                1,
                token.token,
                DateTime.Now,
                DateTime.Now.AddSeconds(Convert.ToDouble(token.expiresIn)),
                true,
                userDataString);
            return FormsAuthentication.Encrypt(ticket);
        }

        public JsonResult LoadUserLite(int uid,string token)
        {
            var tmpToken = new Token
            {
                expiresIn = "",
                id = uid.ToString(),
                token = token
            };

            JObject answer = JObject.Parse(
                _vkApiWraper.APIUsersGet(
                    uid, 
                    tmpToken, 
                    "first_name,last_name,photo_50"
                    ).ToJson()
                );

            var result = new
            {
                first_name = answer["first_name"].Value<string>(),
                last_name = answer["last_name"].Value<string>(),
                photo_50 = answer["photo_50"].Value<string>()
            };
            System.Threading.Thread.Sleep(400);

            return Json(result,JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadUserFull(int uid, string token)
        {
         var tmpToken = new Token
            {
                expiresIn = "",
                id = uid.ToString(),
                token = token
            };

            JObject answer = JObject.Parse(
                _vkApiWraper.APIUsersGet(
                    uid,
                    tmpToken,
                    "first_name,last_name,photo_50,photo_200" 
                    ).ToJson()
                );

            var result = new
            {
                first_name = answer["first_name"].Value<string>(),
                last_name = answer["last_name"].Value<string>(),
                photo_50 = answer["photo_50"].Value<string>(),
                photo_200 = answer["photo_200"].Value<string>()
            };
            System.Threading.Thread.Sleep(400);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}