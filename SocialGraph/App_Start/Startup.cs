﻿using System;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SocialGraph.Startup))]

namespace SocialGraph
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
