﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json.Linq;
using SocialGraph.Models;
using SocialGraph.Util;

namespace SocialGraph.Hubs
{
    public class MainHub : Hub
    {
        private VkAPIWraper _vkApiWraper = new VkAPIWraper();

        public void GetFriendPack(string user, string[] friends, string token)
        {
            int count = friends.Length;
            int current = 0;
            var numOfParts = count/25;
            var targets = new StringBuilder();
            var friendPack = new List<List<string>>();
            for (int i = 0; i < numOfParts; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    targets.Append(friends[current]);
                    targets.Append(',');
                    current += 1;
                }
                targets.Remove(targets.Length - 1, 1);

                friendPack.AddRange(_vkApiWraper.APIExecuteGetFriendPack(targets.ToString(), token));

                //Вот тут вернуть пакет друзей
                Clients.Caller.draw(_vkApiWraper.FriendPackToJSON(friendPack, friends, user,current - 25).ToString(),"lite");

                Clients.Caller.percent((current * 100) / count);
                friendPack.Clear();
                System.Threading.Thread.Sleep(400);
                targets.Clear();
            }

            int derive = count - current;
            if (derive == 0) return;

            for (int i = 0; i < derive; i++)
            {
                targets.Append(friends[current]);
                targets.Append(',');
                current += 1;
            }
            targets.Remove(targets.Length - 1, 1);

            friendPack.AddRange(_vkApiWraper.APIExecuteGetFriendPack(
                targets.ToString(), token)
                );

            //Вот тут вернуть пакет друзей
            Clients.Caller.draw(_vkApiWraper.FriendPackToJSON(friendPack, friends, user, current - derive).ToString(),"lite");
            Clients.Caller.percent((current * 100) / count);
            targets.Clear();
        }

        public void LoadFriend(string id, string token)
        {
            List<LiteUser> friends = _vkApiWraper.APIFriendsGet(id, token);
            var friendsArray = new string[friends.Count];
            for (int i = 0; i < friends.Count; i++)
            {
                friendsArray[i] = friends[i].id;
            }

            var jObject = new JObject();
            var jArray = new JArray();
            foreach (var jObjectUser in friends.Select(liteUser => new JObject { { liteUser.id, liteUser.sex } }))
            {
                jArray.Add(jObjectUser);
            }
            jObject.Add(id, jArray);

            Clients.Caller.draw(jObject.ToString(),"full");

            GetFriendPack(id, friendsArray, token);
        }
    }
}