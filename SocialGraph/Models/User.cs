﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using Newtonsoft.Json;

namespace SocialGraph.Models
{
    public class User
    {
        [JsonProperty(PropertyName = "user_id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "first_name")]
        public string firstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string lastName { get; set; }

        [JsonProperty(PropertyName = "sex")]
        public string sex { get; set; }

        public int friendConter { get; set; }

        public List<LiteUser> friends { get; set; }
        
        [JsonProperty(PropertyName = "photo_50")]
        public string photo_50 { get; set; }

        [JsonProperty(PropertyName = "photo_200")]
        public string photo_200 { get; set; }

        public Token token { get; set; }
    }

    public class Token
    {
        [JsonProperty(PropertyName = "access_token")]
        public string token { get; set; }
        [JsonProperty(PropertyName = "expires_in")]
        public string expiresIn { get; set; }
        [JsonProperty(PropertyName = "user_id")]
        public string id { get; set; }
    }

    public class LiteUser
    {
        [JsonProperty(PropertyName = "id")]
        public string id { get; set; }
        [JsonProperty(PropertyName = "sex")]
        public string sex { get; set; }
    }
}